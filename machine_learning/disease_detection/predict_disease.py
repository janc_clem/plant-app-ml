from PIL import Image
import json
import torch
from .disease_network_CNN import Disease_Network_CNN
from torchvision import transforms
import os


def load_model(model_path, target_size):
    model = Disease_Network_CNN(target_size)
    model.load_state_dict(torch.load(model_path, map_location='cpu'))
    model.eval()
    return model


def load_reference(path):
    with open(path, 'r') as f:
        labels = json.load(f)
    return labels


def predict(img_path, model, labels):
    image = Image.open(img_path)
    test_transform = transforms.Compose([transforms.Resize(255), transforms.CenterCrop(224), transforms.ToTensor()])
    y_result = model(test_transform(image).unsqueeze(0))
    probs = torch.nn.functional.softmax(y_result, dim=1)
    conf, result_idx = torch.max(probs, 1)
    return next((key for key, value in labels.items() if (value == result_idx)), "Unknown"), float(conf)


def get_disease_prediction(img_path):
    script_location = (os.path.dirname(os.path.abspath(__file__)))
    model_path = os.path.join(script_location, "Models/plant_disease_model_12epoch.pt")
    labels = load_reference(os.path.join(script_location, "labels.json"))
    model = load_model(model_path, len(labels))
    return predict(img_path, model, labels)


def main():
    img_path = "machine_learning/disease_detection/Datasets/Plant_leaf_diseases_dataset_with_augmentation/Plant_leave_diseases_dataset_with_augmentation/Tomato___Bacterial_spot/image (30).JPG"
    result, confidence = get_disease_prediction(img_path)
    if "___" in result:
        plant, disease = result.split("___")
        print(f"Plant: {plant} \t Disease: {disease} \t Confidence: {confidence*100:.5f}%")
    else:
        print(result)


if __name__ == "__main__":
    main()
